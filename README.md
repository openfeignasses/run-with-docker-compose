# run-with-docker-compose

Repository containing instructions to run Cryptobot with docker-compose.

## Requirements

- docker-compose

*Commands must be run from the root of this repo.*  
*Current folder must live next by the other modules:*  
```
.
├── run-with-docker-compose
├── data-provider
├── strategies
├── allocations
├── database-migrator
```

## Run

```
docker-compose up
```

## Database access

After having modules running with docker-compose, you maybe want to have a look at the data.

1. Find the IP of the running database by using `docker ps` then `docker inspect`
2. Use this IP and username / password from docker-compose file to connect using a database client